/*  rsa_engine.hpp - Declaration of the RSA engine using GNU Multiple Precision Arithmetic Library (GMP).
 *  This file was a standalone before I added it to RSAGEN and can work as such if desired alongside rsa_engine.cpp.

    RSAGEN - Qt application to generate, load and save RSA keys and
    encode/decode strings with the RSA encryption.

    Copyright (C) 2018 Jaume Fuster i Claris

    This file is part of RSAGEN.

    RSAGEN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RSAGEN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RSAGEN inside the file "LICENSE".
    If not, see <https://www.gnu.org/licenses/gpl.html>.

*/


// Because this file can be used as a standalone, maybe saving someone's headache
#pragma once


#include <iostream>
#include <limits.h>
#include <cstdlib>
#include <cmath>
#include <gmp.h>


class rsa_c {

    private:

        // Random prime generator with n bits
        void randomPrime(mpz_t rpg, unsigned int bits);

    public:

        // Public key struct
        struct public_key_s {
            bool loaded = false;
            mpz_t n, e;
        } public_key;

        // Private key struct
        struct private_key_s {
            bool loaded = false;
            mpz_t n, d;
        } private_key;

        // Constructor
        rsa_c();

        // Randomize key pair
        void randomize(unsigned int bits = 32, unsigned int seed = 0);
        // Set public key from 2 strings
        void setPublicKey(std::string N, std::string E);
        // Set private key from 2 strings
        void setPrivateKey(std::string N, std::string D);
        // Call mpz_clears on public and private key structs and false it's loaded boolean
        void clear();

};

// Encrypt method to string with public key
std::string rsaEncrypt(rsa_c::public_key_s pk, std::string str);
// Decrypt method to string with private key
std::string rsaDecrypt(rsa_c::private_key_s pk, std::string str);
