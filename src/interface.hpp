/*  interface.hpp - Declaration of the Qt user interface main components.

    RSAGEN - Qt application to generate, load and save RSA keys and
    encode/decode strings with the RSA encryption.

    Copyright (C) 2018 Jaume Fuster i Claris

    This file is part of RSAGEN.

    RSAGEN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RSAGEN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RSAGEN inside the file "LICENSE".
    If not, see <https://www.gnu.org/licenses/gpl.html>.

*/


#include <QApplication>
#include <QPushButton>
#include <QMainWindow>
#include <QStatusBar>
#include <QTextBlock>
#include <QTextEdit>
#include <QMenuBar>
#include <QToolBar>
#include <QWidget>
#include <QAction>
#include <QtGui>
#include <QIcon>
#include <QMenu>

#include "rsa_engine.hpp"


// rsa_window base width
#define W 600
// rsa_window base heigth
#define H 300
// QTextUpdate margin
#define M 6
// Max length displayed in rsa_window::prk and rsa_window::puk
#define MAX_KEY_LEN 29

// Help links
#define HELP_ABOUT "https://rsagen.capuno.cat/"
#define HELP_LICENSE "https://rsagen.capuno.cat/#license"
#define HELP_RSA "https://rsagen.capuno.cat/tutorials#rsa"

// QTextUpdate id
#define TXT_EVENT 1
#define RSA_EVENT 2


// This is for both QTextEdit, to hook keyPressEvent and update the other one
class QTextUpdate;
// This is the main QMainWindow
class rsa_window;

class QTextUpdate : public QTextEdit {

    private:

        // TXT_EVENT / RSA_EVENT
        uint8_t id;
        // Parent window pointer
        rsa_window *prwindow;

    public:

        // Base constructor, also calls QTextEdit constructor
        QTextUpdate(rsa_window * parent, const uint8_t id);
        // Key press event hook, calls base QTextEdit::keyPressEvent, regex for RSA_EVENT, and updates the other QTextUpdate
        virtual void keyPressEvent(QKeyEvent * ev);

};

class rsa_window : public QMainWindow {

    Q_OBJECT

    public slots:

        // FILE
        void loadpu();
        void savepu();
        void loadpr();
        void savepr();

        // KEY
        void randomize();
        void unload();
        void setbits();
        void setseed();

        // HELP
        void about();
        void license();
        void aboutrsa();

    private:

        // QActions in class scope for dynamic de/activation
        QAction *savepuQA, *saveprQA, *unloadQA;
        // Input fields
        QTextUpdate *txtio, *rsaio;
        // Info about current key pairs
        QLabel *prk, *puk;
        // Base values for random key generation
        unsigned int rsabits = 64, seed = 0;

    public:

        // The RSA engine main object, definition at rsa_engine.hpp
        rsa_c rsae;
        // Update QTextUpdate fields
        void updateio(uint8_t id);
        // Constructor
        rsa_window();
        // Resize Event for rsa_window
        virtual void resizeEvent(QResizeEvent* event);

};
