
The files in this folder are the following:

- rsagen.1
	The manpage for rsagen, installed with "sudo make install" in /usr/local/man/man1/rsagen.1

- rsagen.png
	The icon for rsagen, installed with "sudo make install" in /usr/local/share/icons/hicolor/32x32/apps/

Note: All the files can be removed from the system with "sudo make uninstall" in the build folder.
